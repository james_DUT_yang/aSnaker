<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.util.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="stylesheet" href="./bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="./bootstrap/css/bootstrap-theme.min.css">
  <script src="./bootstrap/js/jquery-1.10.0.min.js"></script>
  <script src="./bootstrap/js/bootstrap.min.js"></script>
<title>英雄联盟英雄添加</title>
</head>
<body>
<div class="container">
<fieldset style="width:500px">
	<legend>league of legend</legend>
	<form action="${pageContext.request.contextPath}/servlet/lolherosuploadservlet" enctype="multipart/form-data" method="post">
		<table>
			<tr>
				<td>Hero's Nick Name</td>
				<td><input type="text" name="heronickname" size="20" maxlength="20"></td>
			</tr>
			<tr>
				<td>Hero's CN Name</td>
				<td><input type="text" name="heronamecn" size="20" maxlength="20"></td>
			</tr>
			<tr>
				<td>Hero's EN Name</td>
				<td><input type="text" name="heronameen" size="20" maxlength="20"></td>
			</tr>
			<tr>
				<td>Hero's Type</td>
				<td><select name="herotype">
					<option value="射手">射手</option>
					<option value="坦克">坦克</oprion>
					<option value="刺客">刺客</option>
					<option value="法师">法师</option>
					<option value="辅助">辅助</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>英雄头像</td>
				<td><input name="heroheadpic" type="file"></td>
			</tr>
			<tr>
				<td>英雄声音</td>
				<td><input name="herosound" type="file"></td>
			</tr>
			<tr>
				<td>Hero's Story</td>
				<td>
				<textarea name="story" cols="34" >
				</textarea>
				</td>
			</tr>

			<tr>
				<td><input type="submit" value="提交"></td>
				<td><input type="reset" valuee="重置"></td>
			</tr>
		</table>
	</form>
	<a href="http://localhost:8081/test4/ListHeros">ListHeros</a>
</fieldset>
</div>
</body>
</html>