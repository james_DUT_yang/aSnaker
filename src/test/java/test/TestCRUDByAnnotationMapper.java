package test;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

import mybatis.entities.LoLHeros;
import mybatis.entities.User;
import mybatis.mapping.LoLHerosMapper;
import mybatis.mapping.UserMapperI;
import mybatis.util.MyBatisUtil;

/**
 *
 * @date 2015年11月9日 下午3:05:22
 * @author James Yang
 * @version 1.0
 * @since
 */
public class TestCRUDByAnnotationMapper {
	
	@Test 
	public void testAdd(){
		SqlSession sqlSession = MyBatisUtil.getSqlSession(true);
		//得到UserMapperI接口的实现类对象，UserMapperI接口的实现类对象由sqlSession.getMapper(UserMapperI.class)动态构建出来
		UserMapperI mapper = sqlSession.getMapper(UserMapperI.class);
		User user = new User();
		user.setName("阿兹尔");
		user.setAge(24);
		int add = mapper.add(user);
		//使用SqlSession执行完SQL之后需要关闭SqlSession
		sqlSession.close();
		System.out.println(add);
	}
	
	@Test
	public void testUpdate(){
		SqlSession sqlSession = MyBatisUtil.getSqlSession(true);
		UserMapperI mapper = sqlSession.getMapper(UserMapperI.class);
		User user = new User();
		user.setId(5);
		user.setName("费德提克");
		user.setAge(100);
		int update = mapper.update(user);
		sqlSession.close();
		System.out.println(update);
	}
	
	@Test
	public void testDelete(){
		SqlSession sqlSession = MyBatisUtil.getSqlSession(true);
		UserMapperI mapper = sqlSession.getMapper(UserMapperI.class);
		int retResult = mapper.deleteById(5);
		sqlSession.close();
		System.out.println(retResult);
	}
	
	@Test
	public void testGetUser(){
		SqlSession sqlSession = MyBatisUtil.getSqlSession(true);
		UserMapperI mapper = sqlSession.getMapper(UserMapperI.class);
		User user= mapper.getById(3);
		sqlSession.close();
		System.out.println(user);
	
	}
	
	@Test
	public void testGetAll(){
		SqlSession sqlSession = MyBatisUtil.getSqlSession(true);
		UserMapperI mapper = sqlSession.getMapper(UserMapperI.class);
		List<User> lstUsers = mapper.getAll();
		sqlSession.close();
		System.out.println(lstUsers);
	}
	@Test
	public void testGetAllHeros(){
		SqlSession sqlSession = MyBatisUtil.getSqlSession(true);
		LoLHerosMapper mapper = sqlSession.getMapper(LoLHerosMapper.class);
		List<LoLHeros> lstHeros = mapper.getAll();
		sqlSession.close();
		System.out.println(lstHeros);
		
	}
	@Test
	public void testGetHerosbyname(){
		SqlSession sqlSession = MyBatisUtil.getSqlSession(true);
		LoLHerosMapper mapper = sqlSession.getMapper(LoLHerosMapper.class);
		LoLHeros hero = mapper.getByName("沙漠皇帝");
		sqlSession.close();
		System.out.println(hero);
		
	}
	@Test 
	public void testAddhero(){
		SqlSession sqlSession = MyBatisUtil.getSqlSession(true);
		LoLHerosMapper mapper = sqlSession.getMapper(LoLHerosMapper.class);
		LoLHeros hero = new LoLHeros();
		hero.setNamecn("阿兹尔");
		hero.setNameen("azier");
		hero.setType("刺客");
		int add = mapper.add(hero);
		//使用SqlSession执行完SQL之后需要关闭SqlSession
		sqlSession.close();
		System.out.println(add);
	}
}
