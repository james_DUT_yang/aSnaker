package mybatis.entities;

import java.sql.Blob;

/**
 * lolheros表所对应的实体类
 * @date 2015年11月10日 下午2:48:44
 * @author James Yang
 * @version 1.0
 * @since
 */
public class LoLHeros {
	private int id;
	private String nickname; 
	private String nameen;
	private String namecn;
	private String type;
	private String story;
	private String headpicurl;
	private String soundsurl;
	//private Blob headpic;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNameen() {
		return nameen;
	}
	public void setNameen(String nameen) {
		this.nameen = nameen;
	}
	public String getNamecn() {
		return namecn;
	}
	public void setNamecn(String namecn) {
		this.namecn = namecn;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getStory() {
		return story;
	}
	public void setStory(String story) {
		this.story = story;
	}
	
public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public String getHeadpicurl() {
		return headpicurl;
	}
	public void setHeadpicurl(String headpicurl) {
		this.headpicurl = headpicurl;
	}
	public String getSoundsurl() {
		return soundsurl;
	}
	public void setSoundsurl(String soundsurl) {
		this.soundsurl = soundsurl;
	}
	//	public Blob getHeadpic() {
//		return headpic;
//	}
//	public void setHeadpic(Blob headpic) {
//		this.headpic = headpic;
//	}
	@Override
	public String toString() {
		System.out.println("LoLHeros:"
				+ this.getNickname()+":"
				+ this.getNamecn()+":"
				+ this.getNameen()+":"
				+ this.getType()+":"
				+ this.getStory()+":"
				+ this.getHeadpicurl()+":"
				+ this.getSoundsurl());
		return super.toString();
	}
	
}
