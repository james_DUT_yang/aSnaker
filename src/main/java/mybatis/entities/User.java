package mybatis.entities;
/**
 * user表所对应的实体类
 * @date 2015年11月9日 上午11:31:54
 * @author James Yang
 * @version 1.0
 * @since
 */
public class User {
	//实体类的属性和表的字段名称一一对应
	private int id;
	private String name;
	private int age;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	@Override
	public String toString(){
		return "User [id="+id+",name="+name+",age="+age+"]";
	}
	
}
