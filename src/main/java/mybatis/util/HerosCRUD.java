package mybatis.util;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

import mybatis.entities.LoLHeros;
import mybatis.entities.User;
import mybatis.mapping.LoLHerosMapper;
import mybatis.mapping.UserMapperI;

/**
 *  Heros表的crud操作
 * @date 2015年11月11日 下午5:45:01
 * @author James Yang
 * @version 1.0
 * @since
 */
public class HerosCRUD {
	/**
	 * 添加heros操作
	 * @param hero
	 * @return
	 */
	@Test
	public static int addHero(LoLHeros hero){
		SqlSession sqlSession = MyBatisUtil.getSqlSession(true);
		LoLHerosMapper mapper = sqlSession.getMapper(LoLHerosMapper.class);
		
		int add = mapper.add(hero);
		//使用SqlSession执行完SQL之后需要关闭SqlSession
		sqlSession.close();
		//System.out.println(add);
		return add;
	}
	/**
	 * 根据id获取heros
	 * @param id
	 * @return
	 */
	public static LoLHeros getHeroById(int id){
		SqlSession sqlSession = MyBatisUtil.getSqlSession(true);
		LoLHerosMapper mapper = sqlSession.getMapper(LoLHerosMapper.class);
		LoLHeros heros = mapper.getById(id);
		//使用SqlSession执行完SQL之后需要关闭SqlSession
		sqlSession.close();
		System.out.println(heros.toString());
		return heros;
	}
	/**
	 * 根据namecn获取heros
	 * @param id
	 * @return
	 */
	public static LoLHeros getHeroBynamecn(String namecn){
		SqlSession sqlSession = MyBatisUtil.getSqlSession(true);
		LoLHerosMapper mapper = sqlSession.getMapper(LoLHerosMapper.class);
		LoLHeros heros = mapper.getByName(namecn);
		//使用SqlSession执行完SQL之后需要关闭SqlSession
		sqlSession.close();
		System.out.println(heros.toString());
		return heros;
	}
	public static List<LoLHeros> getAllHeros(){
		SqlSession sqlSession = MyBatisUtil.getSqlSession(true);
		LoLHerosMapper mapper = sqlSession.getMapper(LoLHerosMapper.class);
		List<LoLHeros> heroslist = mapper.getAll();
//		for(LoLHeros hero : heroslist){
//			System.out.println(hero.toString());
//		}
		//使用SqlSession执行完SQL之后需要关闭SqlSession
		sqlSession.close();
		return heroslist;
	}
}
