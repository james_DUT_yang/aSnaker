package mybatis.util;

import java.io.InputStream;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

/**
 *
 * @date 2015年11月9日 下午1:57:22
 * @author James Yang
 * @version 1.0
 * @since
 */
public class MyBatisUtil {
	/**
	 * 获取SqlSessionFactory
	 * @return
	 */
	public static SqlSessionFactory getSqlSessionFactory(){
		String resource = "conf2.xml";
		InputStream is = MyBatisUtil.class.getClassLoader().getResourceAsStream(resource);
		SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(is);
		return factory;
		
	}
	/**
	 * 获取sqlsession
	 * @return
	 */
	public static SqlSession getSqlSession(){
		return getSqlSessionFactory().openSession();
	}
	/**
	 * 获取Sqlsession
	 */
	public static SqlSession getSqlSession(boolean isAutoCommit){
		return getSqlSessionFactory().openSession(isAutoCommit);
		
	}
}
