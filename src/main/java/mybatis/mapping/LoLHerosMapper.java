package mybatis.mapping;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import mybatis.entities.LoLHeros;
import mybatis.entities.User;

/**
 *	定义sql映射的接口，使用注解指明方法要执行的SQL
 * @date 2015年11月10日 下午3:01:51
 * @author James Yang
 * @version 1.0
 * @since
 */
public interface LoLHerosMapper {
		//使用@Insert注解指明add方法要执行的SQL
		@Insert("insert into lolheros(nickname,name_en,name_cn,type,story,headpic_url,sounds_url) values(#{nickname},#{nameen},#{namecn},#{type},#{story},#{headpicurl},#{soundsurl})")
		public int add(LoLHeros lolheros);
		
		//使用@Delete注解指明deleteById方法要执行的SQL
		@Delete("delete from lolheros where id=#{id}")
		public int deleteById(int id);
		
		//使用@Update注解指明update方法要执行的SQL
		@Update("update lolheros set nickname=#{nickname},name_en=#{nameen},name_cn=#{namecn},type=#{type},story=#{story},headpic_url=#{headpicurl},sounds_url=#{soundsurl} where id=#{id}")
		public int update(User user);
		
		//使用@Seletct注解指明getById方法要执行的SQL
		@Select("select nikename nikename,name_en nameen,name_cn namecn,type type,story story,headpic_url headpicurl,sounds_url soundsurl from lolheros where id=#{id}")
		public LoLHeros getById(int id);
		
		//使用@Seletct注解指明getByName方法要执行的SQL
		@Select("select nikename nikename,name_en nameen,name_cn namecn,type type,story story,headpic_url headpicurl,sounds_url soundsurl from lolheros where name_cn=#{namecn}")
		public LoLHeros getByName(String namecn);
		
		//使用@Select注解指明getAll方法要执行的SQL
		@Select("select id id,nickname nickname,name_en nameen,name_cn namecn,type type,story story,headpic_url headpicurl,sounds_url soundsurl from lolheros")
		public List<LoLHeros> getAll();
}
