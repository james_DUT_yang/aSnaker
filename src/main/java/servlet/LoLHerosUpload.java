package servlet;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Blob;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.http.fileupload.FileItem;
import org.apache.tomcat.util.http.fileupload.FileUploadException;
import org.apache.tomcat.util.http.fileupload.RequestContext;
import org.apache.tomcat.util.http.fileupload.disk.DiskFileItemFactory;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;
import org.apache.tomcat.util.http.fileupload.servlet.ServletRequestContext;

import mybatis.entities.LoLHeros;
import mybatis.util.HerosCRUD;

/**
 *	处理lolheros的上传信息
 * @date 2015年11月10日 下午3:18:12
 * @author James Yang
 * @version 1.0
 * @since
 */
@WebServlet(name="LoLHerosUploadServlet",urlPatterns="/servlet/lolherosuploadservlet")
public class LoLHerosUpload extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		super.doGet(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setCharacterEncoding("utf-8");
		resp.setContentType("text/html;charset=utf-8");
		PrintWriter out = resp.getWriter();
	/**
	 * 如果表单采用enctype="multipart/form-data"这种方式，将不能用如下注释方式解析	 
	 */
//		String heronamecn = req.getParameter("heronamecn");
//		String heronameen = req.getParameter("heronameen");
//		String herotype = req.getParameter("herotype");
//		//Blob heroheadpic = req.getParameter("heroheadpic")
//		String story = req.getParameter("story");
//		System.out.println(heronamecn+heronameen+herotype+story);
		//得到上传文件的保存目录，将上传的文件保存在WEB-INF目录下
		Path savePath = Paths.get(this.getServletContext().getRealPath("/assets/images/champions"));
		//创建上传文件的目录
		//if(savePath)
		//System.out.println("创建目录"+savePath);
		//Path saveDir =  Files.createDirectories(savePath);
		//out.println("<strong>"+savePath+"创建了出来"+"</strong>");		
		
		//保存上传的文件
		DiskFileItemFactory factory = new DiskFileItemFactory();
		//System.out.println("factory:"+factory);
		ServletFileUpload upload = new ServletFileUpload(factory);
		//System.out.println("upload:"+upload);
		upload.setHeaderEncoding("UTF-8");
		//判断提交的数据是否是上传表单的数据，并且设置了multipart
		if(!ServletFileUpload.isMultipartContent(req)){
			//如果不是就退出
			return;
		}
		List<FileItem> list = null;
		try {
			list = upload.parseRequest(new ServletRequestContext(req));
		} catch (FileUploadException e) {
			System.out.println("解析错误:"+upload.toString());
			e.printStackTrace();
		}
		LoLHeros hero = new LoLHeros();
		for(FileItem item : list){
			if(item.isFormField()){
				switch (item.getFieldName()){
					case "heronickname":
						hero.setNickname(item.getString("UTF-8"));
						continue;	
					case "heronamecn":
						hero.setNamecn(item.getString("UTF-8"));
						continue;
					case "heronameen":
						hero.setNameen(item.getString("UTF-8"));
						continue;
					case "herotype":
						hero.setType(item.getString("UTF-8"));
						continue;
					case "story":
						hero.setStory(item.getString("UTF-8"));
						continue;
					case "heroheadpic":
						hero.setHeadpicurl(item.getString("UTF-8"));
						continue;
					case "herosound":
						hero.setSoundsurl(item.getString("UTF-8"));
						continue;
					default :
						out.write("缺少字段");
				}
			}else{
				String filename = item.getName();
				String fileldname = item.getFieldName();
				if(filename==null || filename.trim().equals("")){
					continue;
				}
				switch(fileldname){
					case "heroheadpic":
						hero.setHeadpicurl(filename);
						break;
					case "herosound":
						hero.setSoundsurl(filename);
						break;
				}
				//注意：不同的浏览器提交的文件名是不一样的，有些浏览器提交上来的文件名是带有路径的，如：  c:\a\b\1.txt，而有些只是单纯的文件名，如：1.txt
                //处理获取到的上传文件的文件名的路径部分，只保留文件名部分
                filename = filename.substring(filename.lastIndexOf("\\")+1);
                InputStream in = item.getInputStream();
                FileOutputStream output = new FileOutputStream(savePath+System.getProperty("file.separator")+filename);
                byte buffer[] = new byte[1024];
                int len =0;
                while((len=in.read(buffer))>0){
                	output.write(buffer,0,len);
                }
                in.close();
                output.close();
                //删除处理文件上传时生成的临时文件
                item.delete();
			}
			

		}
		int a = HerosCRUD.addHero(hero);
		if(a>0){
			out.println("添加成功");
		}else{
			out.println("添加失败");
		}
		

		
		out.println("<!DOCTYPE HTML>");
		    out.println("<HTML>");
		    out.println(" <HEAD>");
		    out.println("<TITLE>LoLHeros Servlet</TITLE>");
		    out.println("<meta http-equiv=\"content-type\" " + "content=\"text/html; charset=utf-8\">");
		    out.println("<meta http-equiv='refresh' content='3;url=http://localhost:8081/test4/ListHeros'>");
		    out.println("</HEAD>");
		    out.println("<BODY>");
		    out.println("upload succesed");
		    out.println("将跳转到servlet:listheros");
//		    out.println(hero.getNamecn());
//		    out.println(hero.getNameen());
//		    out.println(hero.getType());
//		    out.println(hero.getStory());
//		    out.println(hero.toString());
		    out.println(" </BODY>");
		    out.println("</HTML>");
		    out.flush();
		    out.close();
		    //super.doPost(req, resp);
	}
	
}
