package servlet;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mybatis.entities.LoLHeros;
import mybatis.util.HerosCRUD;

/**
 * 列出已经上传的英雄信息
 * @author yangzhene
 *
 */
@WebServlet(name="ListHeros",urlPatterns="/ListHeros")
public class ListHeros extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ListHeros() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//获取上传文件的目录
		Path uploadFilePath = Paths.get(this.getServletContext().getRealPath("/assets/images/champions/"));//this.getServletContext().getRealPath("/assets/images/champions/"));//D:\\英雄联盟\\Air\\assets\\images\\champions"
		//获取文件名
		Map<String,String> fileNameMap = new HashMap<String,String>();
		listfile(uploadFilePath,fileNameMap);
		PrintWriter out = response.getWriter();
		Set<String> keyset = fileNameMap.keySet();
		this.getServletContext(); 
		
		request.setAttribute("fileNameMap", fileNameMap);
		request.setCharacterEncoding("utf-8");
		request.getRequestDispatcher("/listheros.jsp").forward(request, response);
	}
	/**
	 * 递归遍历指定目录下的所有文件
	 * @param uploadFilePath
	 * @param fileNameMap
	 */
	private void listfile(Path path, Map<String, String> map) {
		//检查该path是否为一个目录
		if(Files.isDirectory(path)){
			File files[] = path.toFile().listFiles();
			for(File f : files){
				listfile(f.toPath(),map);
			}
		}else{
			Path filename = path.getFileName();
			map.put(filename.toString(),filename.toString());

//			if(filename.toString().endsWith(".mp3")){
//				map.put("sound", filename.toString());
//			}else if(filename.toString().endsWith(".png")){
//				map.put("pic", filename.toString());
//			}else{
//				map.put(filename.toString(),filename.toString());
//			}
		}
				
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<LoLHeros> list = HerosCRUD.getAllHeros();
		request.setAttribute("allheroslist", list);
		request.setCharacterEncoding("utf-8");
		request.getRequestDispatcher("/listheros.jsp").forward(request, response);
		//doGet(request, response);
	}

}
