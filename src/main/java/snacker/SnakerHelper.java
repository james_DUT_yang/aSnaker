package snacker;

import javax.sql.DataSource;

import org.snaker.engine.SnakerEngine;
import org.snaker.engine.access.jdbc.JdbcHelper;
import org.snaker.engine.cfg.Configuration;

/**
 * 2016年10月10日 下午3:26:59
 * Snaker引擎帮助类 
*/

public class SnakerHelper{
	private static final SnakerEngine engine;
	static{
		DataSource dataSource = JdbcHelper.getDataSource();
		engine = new Configuration()
				.initAccessDBObject(dataSource)
				.buildSnakerEngine();
	}
	public static SnakerEngine getEngine(){
		return engine;
	}
}
